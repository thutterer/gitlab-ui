export const DUO_CODE_SCRIM_TOP_CLASS = 'scrim-top';
export const DUO_CODE_SCRIM_BOTTOM_CLASS = 'scrim-bottom';
export const DUO_CODE_SCRIM_OFFSET = 16;
